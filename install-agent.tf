terraform {
  required_providers {
    saggarwal = {
      source = "saggarwal/saggarwal"
      version = "~> 0.1.0"
    }
  }
}

provider "saggarwal" {}

# Create an agent access token
resource "agent_access_token" "ssm_deployment" {
  name        = "ssm-deployment"
  description = "Used to deploy agents using AWS System Manager"
}

# Create AWS SSM Document
module "aws_ssm_agents_install" {
  source = "saggarwal/ssm-agent/aws"
  version = "~> 0.1"

  access_token = agent_access_token.ssm_deployment.token
}

# Create an AWS Resource group for EC2 Instances
resource "aws_resourcegroups_group" "monitoring" {
  name = "agent-candidates"

  resource_query {
    query = jsonencode({
      ResourceTypeFilters = [
        "AWS::EC2::Instance"
      ]

      TagFilters = [
        {
          Key = "billing_code"
          Values = [
            var.billing_code
          ]
        }
      ]
    })
  }
}

# Create an SSM Association group
resource "aws_ssm_association" "aws_ssm_agents_install_testing" {
  association_name = "install-agents-monitoring-group"

  name = module.aws_ssm_agents_install.ssm_document_name

  targets {
    key = "resource-groups:Name"
    values = [
      aws_resourcegroups_group.monitoring.name,
    ]
  }

  compliance_severity = "HIGH"
}

