variable "aws_access_key" {
    type = string
    description = "AWS access key"
    sensitive = true
}

variable "aws_secret_key" {
    type = string
    description = "AWS secret key"
    sensitive = true
}

variable "aws_region" {
    type = string
    description = "AWS region"
    default = "eu-west-3"
    sensitive = false
}

variable "instance_type" {
    type = string
    default = "t2.micro"

    validation {
        condition = contains(["t2.micro", "t3.micro", "t3.small", "t2.medium", "t2.small"])
        error_message = "The instance type is invalid."
    }
}

variable "subnet" {
    type = string
    default = "subnet-005078b0afdf1b53a"
}

variable "aws_vpc" {
    type = string
    default = "vpc-0c561de5b4626a97f"
}

variable "aws_vpc_cidr_block" {
    type = string
    default = "10.0.0.0/16"
}

variable "billing_code" {
    type = string
    description = "Billing code for resource tagging"
}
