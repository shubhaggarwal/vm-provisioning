data "aws_ssm_parameter" "ami" {
  name = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}

resource "aws_instance" "nginx" {
  count                  = 200     
  ami                    = nonsensitive(data.aws_ssm_parameter.ami.value)
  instance_type          = var.instance_type
  subnet_id              = var.subnet
  vpc_security_group_ids = [aws_security_group.nginx-sg.id]
  tags = merge({Name = "Nginx-${count.index}"}, local.common_tags)
}
